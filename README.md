# README #

## Intention
Write a professional quality piece of code that reads English text from any text file and efficiently finds all the words that are the reverse of other words, and prints these out as a list sorted by string length with each pair occurring once.

For example the output from a file containing "what he saw was not part of a trap just a ton of snow" would output
saw, was
not, ton
part, trap

## Running jar file
1. Download the repository. 
2. Jar file is located in directory out/artifacts/ReversedWords/ReversedWords.jar
3. Enter the command line command "java -jar ReversedWords.jar ../../../example.txt" such as
c:\Projects\Java\CheckReversedWords\out\artifacts\ReversedWords>java -jar ReversedWords.jar ../../../example.txt

## Command line parameters
* ReversedWords accepts up to 2 parameters:

1. File to process - compulsory parameter
   
2. Delimiter file specifying all the characters that are not considered as words - optional parameter

In case of not specifying the delimiter file, a default whitespaces configuration is being used.