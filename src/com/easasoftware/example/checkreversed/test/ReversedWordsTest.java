package com.easasoftware.example.checkreversed.test;

import com.easasoftware.example.checkreversed.ReversedWords;
import org.testng.annotations.Test;
import org.testng.collections.Lists;
import java.util.Collection;
import java.util.List;
import static org.testng.Assert.*;

public class ReversedWordsTest {

    @Test
    void testProperExample(){
        ReversedWords.main(new String[]{"example.txt"});
    }

    @Test
    void testReverseWord(){
        final String reversedWord = ReversedWords.reverseWord("lama");
        assertEquals(reversedWord, "amal");
    }

    @Test(expectedExceptions = NullPointerException.class)
    void testReverseWordNull(){
        ReversedWords.reverseWord(null);
    }

    @Test
    void testParseToWords(){
        final List<String> expectedWords = Lists.newArrayList("a", "he", "of", "not", "saw", "ton", "was", "just", "part", "snow", "trap", "what");
        final Collection<String> words = ReversedWords.parseToWords("what he saw was not part of a trap just a ton of snow", ReversedWords.DEFAULT_DELIMITERS);
        words.stream().forEach(System.out::println);

        assertEquals(words.size(), 12);
        assertTrue(words.containsAll(expectedWords));
        assertFalse(words.contains("nope"));
    }

    @Test
    void testGetDelimiters(){
        assertEquals(ReversedWords.getDelimiters(null), ReversedWords.DEFAULT_DELIMITERS);
        assertEquals(ReversedWords.getDelimiters("nonexistent.txt"), ReversedWords.DEFAULT_DELIMITERS);
        final String delimiters = ReversedWords.getDelimiters("delim.txt");
        assertTrue(delimiters.contains("/*-+{}[](),.:;"));
        assertTrue(delimiters.contains("”“\"?!0123456789#$%@&●–'"));
        assertTrue(delimiters.contains(" "));
        assertTrue(delimiters.contains("\n"));
        assertTrue(delimiters.contains("\r"));
        assertTrue(delimiters.contains("\\"));
    }

    @Test
    void testFileLoad(){
        final String fileContents = ReversedWords.loadFile("example.txt");
        System.out.println(fileContents);
        assertEquals(fileContents, "what he saw was not part of a trap just a ton of snow");
    }

    @Test
    void testFileExists(){
        assertTrue(ReversedWords.fileExists("example.txt"));
        assertFalse(ReversedWords.fileExists("nonexistent.txt"));
    }

    @Test
    void testNotExistingFile(){
        ReversedWords.main(new String[]{"nonexistent.txt"});
    }

    @Test
    void testInvalidPath(){
        ReversedWords.main(new String[]{"*:--;g"});
    }

    @Test
    void testNoArguments(){
        ReversedWords.main(null);
    }
}
