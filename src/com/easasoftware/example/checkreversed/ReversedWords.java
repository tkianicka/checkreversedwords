package com.easasoftware.example.checkreversed;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TreeSet;

/**
 * ReverseWords class loads a plain text file and prints out all the word pairs that are reverse of each other,
 * ordered by word lenth, in ascending order and ignoring palindromes in current implementation.
 */
public class ReversedWords {

    private static final String CONSOLE_ARGUMENTS_INVALID = "Please run with filename you wish to check as and argument.\n" +
            "Examples: java -jar ReversedWords.jar example.txt\n"+
            "java -jar ReversedWords.jar article.txt delim.txt";
    private static final String INVALID_PATH_EXCEPTION = "Invalid path to a file '%s'\n";
    private static final String FILE_READ_EXCEPTION = "Unable to read file '%s'\n";
    private static final String NO_DELIMITERS_DEFINED = "No delimiter file specified, using default whitespaces delimiters.";
    private static final String DELIMITER_FILE_DOES_NOT_EXIST = "Delimiter file '%s' does not exist. Using default delimiters instead.";

    public static final String DEFAULT_DELIMITERS = " \t\n\r\f";
    private static final Locale TEXT_LOCALE = Locale.ROOT;

    /**
     * Main method
     * @param args expects file name path argument
     */
    public static void main(String[] args) {
        if(!hasValidArguments(args)){
            System.out.println(CONSOLE_ARGUMENTS_INVALID);
            return;
        }
        final String fileName = args[0];        //use first command line argument as text file to load and process
        final String delimitersName = args.length > 1 ? args[1] : null;  //use the second command line parameter if present as delimiter file.
        final String delimiters = getDelimiters(delimitersName);
        final String fileContents = loadFile(fileName);
        if(fileContents != null){
            final Collection<String> words = parseToWords(fileContents, delimiters);
            printReversedWordPairs(words);
        }
    }

    /**
     * Filters and prints expected output on a console
     * @param words String Collection of words
     */
    static void printReversedWordPairs(Collection<String> words) {
        words.stream()
                .filter((s -> words.contains(reverseWord(s))))      //only words that have a reversed word present in the set
                .filter(s -> s.compareTo(reverseWord(s)) < 0)       //remove duplicate entry, keep one lower in ascending order
                                                                    //to allow palindromes, change < 0 to <= 0 in comparison
                .map(s -> s + ", " + reverseWord(s))
                .forEach(System.out::println);
    }

    /**
     * Produces reveresed word, eg. "lama" -> "amal".
     */
    public static String reverseWord(String word){
        return new StringBuilder(word).reverse().toString();
    }

    /**
     * Outputs a collection of String using @see java.util.StringTokenizer, using default deliminator (whitespaces).
     * @param text plain text content with words and whitespaces
     * @param delimiters whitespace characters and punctuation that is not considered as part of a word
     * @return String Collection of words
     */
    public static Collection<String> parseToWords(String text, String delimiters) {
        final StringTokenizer tokenizer = new StringTokenizer(text, delimiters);
        final TreeSet<String> words = new TreeSet<>((o1, o2) -> {
            //compare words length first
            final int lengthCompare = Integer.compare(o1.length(), o2.length());
            if (lengthCompare == 0){
                //same length - compare alphabetically
                return CharSequence.compare(o1, o2);
            }
            return  lengthCompare;
        });
        while(tokenizer.hasMoreTokens()){
            words.add(tokenizer.nextToken().toLowerCase(TEXT_LOCALE));
        }
//        words.stream().forEach(System.out::println);
        return words;
    }

    /**
     * Attempts to loads delimiters from a text file if such file exists, if not it uses default delimiters - whitespaces.
     * @param delimitersFile file path to a text file containing specified word delimiters.
     * @return delimiter configuration string for StringTokenizer
     */
    public static String getDelimiters(String delimitersFile) {
        if(delimitersFile == null){
            System.out.println(NO_DELIMITERS_DEFINED);
            return DEFAULT_DELIMITERS;
        }else if(!fileExists(delimitersFile)){
            System.out.printf(DELIMITER_FILE_DOES_NOT_EXIST, delimitersFile);
            return DEFAULT_DELIMITERS;
        }
        return loadFile(delimitersFile);
    }

    /**
     * @param fileName relative or absolute path to a text file to be loaded as a String.
     * @return true if file exists
     */
    public static boolean fileExists(String fileName){
        try {
            final Path path = Path.of(fileName);
            return Files.exists(path);
        } catch (InvalidPathException e) {
            System.err.printf(INVALID_PATH_EXCEPTION, fileName);
        }
        return false;
    }

    /**
     * @param fileName relative or absolute path to a text file to be loaded as a String.
     * @return file contents as a String
     */
    public static String loadFile(String fileName) {
        try {
            final Path path = Path.of(fileName);
            return Files.readString(path);
        } catch (InvalidPathException e) {
            System.err.printf(INVALID_PATH_EXCEPTION, fileName);
        } catch (IOException e) {
            System.err.printf(FILE_READ_EXCEPTION, fileName);
        }
        return null;
    }

    /**
     * @param args command line arguments array
     * @return true if array has at least one parameter
     */
    static boolean hasValidArguments(String[] args) {
        return args != null && args.length >= 1;
    }
}
